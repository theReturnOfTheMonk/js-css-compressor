var fs = require('fs');
var path = require('path');
//mishoo/UglifyJS2 [https://github.com/mishoo/UglifyJS2]
var UglifyJS = require('uglify-js');
//ryanmcgrath/wrench-js [https://github.com/ryanmcgrath/wrench-js]
var wrench = require('wrench');
/**
    GoalSmashers/clean-css [https://github.com/GoalSmashers/clean-css]

    keepSpecialComments - * for keeping all (default), 1 for keeping first one, 0 for removing all
    keepBreaks - whether to keep line breaks (default is false)
    removeEmpty - whether to remove empty elements (default is false)
    debug - turns on debug mode measuring time spent on cleaning up (run npm run bench to see example)
**/
var cleanCSS = require('clean-css');

var compressor = exports.compressor = function(config){
    //待压缩文件根目录
    var resourceDir = config.resourceDir;
    //压缩文件导出目录.(若不指定, 则创建同级目录"压缩文件导出目录"加"_min")
    var ocDir = config.outputCompressedDir;
    //日志导出目录. (若不指定, 则创建同级目录"压缩文件导出目录"加"_log")
    var olDir = config.outputLogDir;
    //导出文件追加后缀.
    var cName = config.compressedName || '';
    //日志文件名称, 格式为"压缩文件名.日志名"
    var lName = config.logName || 'log';
    //日志文件格式
    var lType = config.logType || '.txt';
    //需要压缩的文件格式.(目前支持js, css)
    var fileType = config.fileType || ['.js'];
    //保存匹配的待压缩文件
    var list = [];
    
    //拷贝目录时参数
    var copyOpt = {
        ignoreFiles : true
    }

    if(config.copyFilter && config.copyFilter.length > 0) {
        copyOpt.filter = new RegExp(config.copyFilter.join('|'), 'gi') 
    }

    //检测文件类型.
    var matchFile = function(ext){
        var flag = false;
        fileType.forEach(function(item){
            if(ext == item) {
                flag = true;
            }
        });
        
        return flag;
    }

    var doCompress = function(itemInfo, list){
        if(itemInfo == undefined) return false;

        var _resourceFile = itemInfo[0];
        var _outputDir = itemInfo[1];
        var ext = path.extname(_resourceFile);
        var basename = path.basename(_resourceFile, ext);
        var result = {};
        var log = '';

        //执行压缩
        switch(ext) {
            case '.js' : 
                result = UglifyJS.minify(_resourceFile, {
                    warnings : true,
                    outputWarnings : true
                });
                log = result.warnMsg.join('\r\n');
            break;
            case '.css' : 
                var source = fs.readFileSync(_resourceFile).toString();
                result.code = cleanCSS.process(source);
            break;
        }

        //写压缩文件
        fs.writeFile(_outputDir + basename + cName + ext, result.code, 'utf-8', function(err, info){
            if(err) {
                console.log(_resourceFile, 'write compressed file error.');
            }

            if(log != '') {
                //写日志
                fs.writeFile(olDir + '[' + ext.substring(1) +']' + basename + '.' + lName + lType, log, 'utf-8', function(err, info){
                    if(err) {
                        console.log(_resourceFile, 'write log error');
                    }

                    console.log(_resourceFile, 'done');
                    //递归调用压缩方法
                    var itemInfo = list.shift();
                    doCompress(itemInfo, list);
                });
            } else {
                console.log(_resourceFile, 'done');
                //递归调用压缩方法
                var itemInfo = list.shift();
                doCompress(itemInfo, list);
            }
        });
    }

    //创建压缩文件, 日志的输出目录(如果未指定)
    var stats = fs.statSync(resourceDir);
    var newOutputDir = resourceDir.slice(0, -1) + '_min/';
    var newLogDir = resourceDir.slice(0, -1) + '_log/';

    if(!ocDir) {
        if(!fs.existsSync(newOutputDir)) {
            fs.mkdirSync(newOutputDir, stats.mode);

        }
        ocDir = newOutputDir;
    }
    //递归复制整个目录结构(忽略所有文件)
    wrench.copyDirSyncRecursive(resourceDir, ocDir, copyOpt);

    if(!olDir) {
        if(!fs.existsSync(newLogDir)) {
            fs.mkdirSync(newLogDir, stats.mode);
        }
        olDir = newLogDir;
    }

    //递归读取所有"待压缩文件根目录"下的所有文件
    var fileList = wrench.readdirSyncRecursive(resourceDir);

    fileList.forEach(function(item){
        var stat = fs.statSync(resourceDir + item);
        var ext = path.extname(resourceDir + item);

        //生成待压缩文件数组
        if(!stat.isDirectory() && matchFile(ext)){
            list.push([resourceDir + item, path.dirname(ocDir + item) + '/']);
        }
    });

    //执行压缩
    var itemInfo = list.shift();
    doCompress(itemInfo, list);
}

//压缩单个文件
var singleCompress = exports.singleCompress = function(filePath, fileName){
    var result = {};
    var log = '';
    if(fileName == undefined || fileName == '' || filePath == '') {
        console.log('FileName:', fileName, '#', 'FilePath:', filePath);
        return result;
    }

    var ext = path.extname(fileName);
    var basename = path.basename(fileName, ext);

    //执行压缩
    switch(ext) {
        case '.js' : 
            result = UglifyJS.minify(filePath, {
                warnings : true,
                outputWarnings : true
            });
            log = result.warnMsg.join('\r\n');
        break;
        case '.css' : 
            var source = fs.readFileSync(filePath).toString();
            result.code = cleanCSS.process(source);
        break;
    }
    
    return result;
}