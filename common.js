/**
    resourceDir: //待压缩文件根目录
    outputCompressorDir: //压缩文件导出目录.(若不指定, 则创建同级目录"压缩文件导出目录"加"_min")
    outputLogDir: //日志导出目录. (若不指定, 则创建同级目录"压缩文件导出目录"加"_log")
    compressorName: //导出文件追加后缀.
    logName: //日志文件名称, 格式为"压缩文件名.日志名"
    logType: //日志文件格式
    fileType: //需要压缩的文件格式.(目前支持js, css)
    copyFilter: //拷贝目录的时候, 需要忽略的文件, 文件夹

    ====================================== help ======================================
    Command : node file -a [ -rd:"" -od:"" -ol:"" -cn:"" -ln:"" -lt:"" -ft:[.js, .css] -cf:[folder1, folder2]]
        -a : auto compress with the default settings

        -rd : directory of resource for compressing
        -od : output directoy of compressed files
        -ol : output directory of log
        -cn : prefix of compressed file (".min")
        -ln : log name ("log")
        -lt : type of log file (.txt, .js...)
        -ft : compressor file type (support css and js now)
        -cf : ignore file/directory when copy a directory

    e.g.
    1. node index.js -a
    2. node index.js -a -rd:./compressFile/newOutput_files/ -ft:[.js,.css]
    ===================================================================================
**/

var handleArgs = exports.handleArgs = function (argv){
    var settings = {
        'resourceDir' : '',
        'outputCompressedDir' : '',
        'outputLogDir' : '',
        'compressedName' : '',
        'logName' : 'log',
        'logType' : '.txt',
        'fileType' : ['.js', '.css'],
        'copyFilter' : []
    };

    if(argv.length == 1) {
        settings.resourceDir = './compressFile/target_files/';
        settings.outputCompressedDir = './compressFile/output_min/';
        settings.outputLogDir = './compressFile/output_logs/';
    }

    argv.forEach(function(item, index){
        var fakeCommand = item.split(':')[0];
        var fakeCommandValue = item.split(':')[1];

        if(fakeCommand == '-rd') {
            settings.resourceDir = fakeCommandValue || './compressFile/target_files/';
        }

        if(fakeCommand == '-od') {
            settings.outputCompressedDir = fakeCommandValue || './compressFile/output_min/';
        }

        if(fakeCommand == '-ol') {
            settings.outputLogDir = fakeCommandValue || './compressFile/output_logs/';
        }

        if(fakeCommand == '-cn') {
            settings.compressedName = fakeCommandValue || '';
        }

        if(fakeCommand == '-ln') {
            settings.logName = fakeCommandValue || 'log';
        }

        if(fakeCommand == '-lt') {
            settings.logType = fakeCommandValue || '.txt';
        }

        if(fakeCommand == '-ft') {
            settings.fileType = fakeCommandValue.replace(/\s|\[|\]/g, '').split(',') || ['.js', '.css'];
        }

        if(fakeCommand == '-cf') {
            settings.copyFilter = fakeCommandValue.replace(/\s|\[|\]/g, '').split(',') || [];
        }
    });

    return settings;
}