//仿佛是这个文件的东西可以再分一分.... 挖坑..
var http = require('http');
var url = require('url');
var config = require('./global_config.js').config;
var router = require('./route/router.js').router;
var req_handler = require('./route/req_handler.js').req_handler;
var compressor = require('./compressor.js').compressor;
var handleArgs = require('./common.js').handleArgs;

var argv = process.argv.slice(2);
if(argv.length > 0) {
    if(argv[0] == '-a' && argv.length > 1) {
        var settingsObj = handleArgs(argv);
        compressor(settingsObj);
    } else {
        console.log(
            ' Arguments error. Please double check the god damn arguments with the help text below\n',
            '====================================== help ======================================\n',
            'Command : node file -a [ -rd:"" -od:"" -ol:"" -cn:"" -ln:"" -lt:[.js, .css] -ft:"" -cf:[folder1, folder2]]\n',
            '    -a : auto compress with the default settings\n',
            '\n',
            '    -rd : directory of resource for compressing\n',
            '    -od : output directoy of compressed files\n',
            '    -ol : output directory of log\n',
            '    -cn : prefix of compressed file (".min")\n',
            '    -ln : log name ("log")\n',
            '    -lt : type of log file (.txt, .js...)\n',
            '    -ft : compressor file type (support css and js now)\n',
            '    -cf : ignore file/directory when copy a directory\n',
            '\n',
            'e.g.\n',
            '1. node index.js -a\n',
            '2. node index.js -a -rd:./compressFile/newOutput_files/ -ft:[.js,.css]\n',
            '==================================================================================='
        );

        return false;
    }
} else {
    http.createServer(function(req, res){
        var pathname = url.parse(req.url).pathname;
        router(req, res, pathname, req_handler, config);
    }).listen(config.port);
}

console.log('Server is running on', config.host + ':' + config.port);
