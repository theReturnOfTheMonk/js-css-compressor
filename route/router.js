var fs = require('fs');
var path = require('path');
var mime = require('./config/mime.js').mime;
var redirect_config = require('./config/redirect_config.js').redirect_config;

var router = exports.router = function(req, res, pathname, req_handler, config){
    //重定向检测
    var _pathname = redirect_config[pathname] != undefined ? redirect_config[pathname] : pathname;
    var basename = path.basename(_pathname);

    if(Object.prototype.toString.call(req_handler[basename]) === '[object Function]') {
        req_handler[basename](req, res, config);
    } else {
        //请求类型, 生成MIME
        var ext = path.extname(_pathname);
        ext = ext ? ext.slice(1) : 'unknown';
        var mimeType = mime[ext] || "text/plain";

        _pathname = config.staticRootDir + _pathname;
        fs.exists(_pathname, function(exists){
            if(!exists) {
                if(ext != '.html') {
                    res.writeHeader(404, {'Content-Type' : 'text/plain'});
                    res.end();
                } else {
                    fs.readFile(config.staticRootDir + '/404.html', 'utf-8', function(err, data){
                        res.writeHeader(404, {'Content-Type' : 'text/html'});
                        res.end(data);
                    });
                }
            } else {
                fs.readFile(_pathname, 'utf-8', function(err, data){
                    if (err) {
                        res.writeHead(500, {'Content-Type': 'text/plain'});
                        res.end(err);
                    } else {
                        res.writeHeader(200, {'Content-Type' : mimeType});
                        res.end(data);
                    }
                });
            }
        });
    }
}