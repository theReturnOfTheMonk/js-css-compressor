var fs = require('fs');
var path = require('path');
var formidable = require('formidable');
var compress = require('../compressor.js');

var req_handler = exports.req_handler = {
    'doSingleCompress' : function(req, res, config){
        var form = new formidable();

        form.parse(req, function(err, fields, files){
            if(err) {
                fs.readFile(config.staticRootDir + '/docompress_single.html', 'utf-8', function(err, data){
                    var realData = data.replace('{{tpl:msg}}', 'Read File error.. S**t.. Try it again, call "110" Or contact the developer...');
                    res.writeHeader(200, {'Content-Type' : 'text/html'});
                    res.end(realData);
                });
            }

            var filePath = form.openedFiles[0] && form.openedFiles[0].path ? form.openedFiles[0].path : '';
            var fileName = form.openedFiles[0] && form.openedFiles[0].name ? form.openedFiles[0].name : '';

            if(filePath && fileName) {
                var ext = path.extname(fileName),
                    basename = path.basename(fileName, ext),
                    timeStamp = generateTimeStamp(),
                    newPath = config.staticRootDir + '/compressedTmp/' + timeStamp + basename + '.old' + ext;

                fs.renameSync(filePath, newPath);

                var result = compress.singleCompress(newPath, fileName);
                if(result.warnMsg) {
                    fs.writeFileSync(config.staticRootDir + '/compressedTmp/' + timeStamp + basename + '.log.txt', result.warnMsg.join('\r\n'));
                }

                if(result.code) {
                    fs.writeFile(config.staticRootDir + '/compressedTmp/' + timeStamp + basename + ext, result.code, function(err){
                        if(err) {
                            throw err;
                        }
                        
                        res.writeHeader(302, {'Location' : config.host + ':' + config.port + '/single'});
                        res.end();
                    });
                }
            }

            res.writeHeader(302, {'Location' : config.host + ':' + config.port + '/single'});
            res.end();
        });
    },
    single : function (req, res, config){
        fs.readFile(config.staticRootDir + '/docompress_single.html', 'utf-8', function(err, data){
            fs.readdir(config.staticRootDir + '/compressedTmp', function(err, files){
                var fileListStr = '<ol id="fileList">';
                var _shortTime = generateTimeStamp('s');
                var flag = '';
                for(var len = files.length, i = len - 1; i >= 0; i -= 1) {
                    var item = files[i];
                    if(i == len - 1) {
                        flag = item.split(')')[0];
                    }
                    var classStr = [];
                    if(item.indexOf(_shortTime) != -1) {
                            classStr.push('today');
                    }

                    if(flag != '' && item.indexOf(flag) != -1) {
                            classStr.push('new');
                    }

                    fileListStr += '<li class="'+ classStr.join(' ') +'"><a href="compressedTmp/'+ item +'" target="_blank">'+ item +'</a></li>';
                }
                fileListStr += '</ol>';

                var realData = updateTpl('single', data, fileListStr);
                res.writeHeader(200, {'Content-Type' : 'text/html'});
                res.end(realData);
            });
        });
    },
    all : function(req, res, config){
        fs.writeFile(config.staticRootDir + '/docompress_all.html', 'utf-8', function(err, data){
            //一个坑... 待填...
            if(err) throw err;

            var realData = updateTpl('all', data, 'test mode "all"');
            res.writeHeader(200, {'Content-Type' : 'text/html'});
            res.end(realData);
        });
    }
}

var generateTimeStamp  = function (s){
    var generatedTimeStamp = '';
    var time = new Date();
    if(s) {
        generatedTimeStamp = time.getFullYear() + '-' +
                ('0' + (time.getMonth() + 1)).slice(-2) + '-' +
                ('0' + time.getDate()).slice(-2) + '='
    } else {
        generatedTimeStamp = '(' + time.getFullYear() + '-' +
                ('0' + (time.getMonth() + 1)).slice(-2) + '-' +
                ('0' + time.getDate()).slice(-2) + '=' +
                ('0' + time.getHours()).slice(-2) + '.' +
                ('0' + time.getMinutes()).slice(-2) + '.' +
                ('0' + time.getSeconds()).slice(-2) + '.' +
                ('00' + time.getMilliseconds()).slice(-3) + ')';
    }

    return generatedTimeStamp;
}

//mode: single||all
var updateTpl = function(mode, data, newData){
    //当前只替换Title和List部分
    var realData = '';
    if(mode == 'single') {
        realData = data.replace('{{tpl:title}}', 'Single Mode - ');
    } else {
        realData = data.replace('{{tpl:title}}', 'All Mode - ');
    }

    return realData.replace('{{tpl:msg}}', newData);
}
